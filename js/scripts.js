$(document).ready(function() {
  $('.fouth-section-top-content').waypoint(function() {

  document.getElementById("progress-1").style.width = "70%"
  document.getElementById("percentage-1").innerHTML = "70%"

  document.getElementById("progress-2").style.width = "90%"
  document.getElementById("percentage-2").innerHTML = "90%"

  document.getElementById("progress-3").style.width = "80%"
  document.getElementById("percentage-3").innerHTML = "80%"

  document.getElementById("progress-4").style.width = "60%"
  document.getElementById("percentage-4").innerHTML = "60%"

  });

  $('.news-section').slick({
    dots: true,
    centerMode: true,
    centerPadding: '60px',
    slidesToShow: 3,
    responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
  });
});